function fetchRandomNumbers(){
    return new Promise((resolve,reject)=>{
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString(){
    return new Promise((resolve,reject)=>{
        console.log('Fetching string...');
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}



// fetchRandomNumbers((randomNum) => console.log(randomNum))
fetchRandomNumbers().then((randomNum) => {
    console.log(randomNum)
    });

// fetchRandomString((randomStr) => console.log(randomStr))
fetchRandomString().then((randomStr) => {
    console.log(randomStr)
    });

// Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable
// -> add it to the same sum variable and print the sum variable.
function sumNum(){
    fetchRandomNumbers().then(randomNum=>{
        sum=0
        i=1
        sum+=randomNum
        console.log(`sum${i} ${sum}`)
        fetchRandomNumbers().then(randomNum=>{
            sum+=randomNum
            i++
            console.log(`sum${i} ${sum}`)
            fetchRandomNumbers().then(randomNum=>{
                sum+=randomNum
                i++
                console.log(`sum${i} ${sum}`)
            })
        })
    })
}
sumNum()

// Fetch a random number and a random string simultaneously, concatenate their
// and print the concatenated string

concatNumStr=()=>{
    data=""
    Promise.all([fetchRandomNumbers(),fetchRandomString()]).
    then(value=>console.log(value.join(" ")))
}
concatNumStr()

// Fetch 10 random numbers simultaneously -> and print their sum
fetchNRandom=(n)=>{
    data=""
    tempArr=[]
    for(let i=0;i<n;i++){
        tempArr.push(fetchRandomNumbers())
    }
    Promise.all(tempArr).
    then(value=>{
        sumOfArr=value.reduce((acc,cur)=>acc+=cur)
        console.log(sumOfArr)
    })
}
fetchNRandom(10)

//async await 

async function sumNumAsync(){
    sum=0
    await fetchRandomNumbers().then(randomNum=>sum+=randomNum)
    console.log(sum)
    await fetchRandomNumbers().then(randomNum=>sum+=randomNum)
    console.log(sum)
    await fetchRandomNumbers().then(randomNum=>sum+=randomNum)
    console.log(sum)
}
sumNumAsync()

concatNumStrAsync=async()=>{
    data=""
    await fetchRandomNumbers().then(x=>data=x)
    await fetchRandomString().then(x=>data+=x)
    console.log(data)
}
concatNumStrAsync()

fetchNRandomAsync=async(n)=>{
    sum=0
    for(let i=0;i<n;i++){
        await fetchRandomNumbers().then(x=>sum+=x)
    }
    console.log(sum)
}
fetchNRandomAsync(10);